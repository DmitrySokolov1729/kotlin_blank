package com.dsokolov.baseapplication.yourapp.common

import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

@Singleton class Preferences @Inject constructor(private val sharedPreferences: SharedPreferences) {
    private val fieldToken = "TOKEN"

    fun saveToken(token: String?) = sharedPreferences.edit().putString(fieldToken, token).apply()
    fun getToken() = sharedPreferences.getString(fieldToken, null)
}