package com.dsokolov.baseapplication.yourapp.logger

import com.dsokolov.baseapplication.yourapp.models.BaseUserModel

class LogConfig(val applicationPackageName: String, val rootPath: String, val pattern: String, val fileCount: Int, val fileSize: Int) : BaseUserModel() {
    override fun getType(): String {
        return this.javaClass.name
    }
}