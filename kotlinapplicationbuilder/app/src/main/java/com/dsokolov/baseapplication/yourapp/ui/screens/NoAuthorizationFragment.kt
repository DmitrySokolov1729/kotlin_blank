package com.dsokolov.baseapplication.yourapp.ui.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.dsokolov.baseapplication.yourapp.ui.screens.base.BaseFragment
import com.dsokolov.baseapplication.databinding.NoAuthorizationBinding

class NoAuthorizationFragment : BaseFragment() {
    private var binding: NoAuthorizationBinding? = null
    private val contactsViewModel: ContactsViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = NoAuthorizationBinding.inflate(inflater, container, false)
        setListeners()
        return binding!!.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun setListeners() {
        binding!!.button.setOnClickListener { contactsViewModel.login() }
    }
}