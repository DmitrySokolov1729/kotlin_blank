package com.dsokolov.baseapplication.yourapp.ui.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dsokolov.baseapplication.databinding.LoadingBinding
import com.dsokolov.baseapplication.yourapp.ui.screens.base.BaseFragment

class LoadingFragment : BaseFragment() {
    private var binding: LoadingBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = LoadingBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}