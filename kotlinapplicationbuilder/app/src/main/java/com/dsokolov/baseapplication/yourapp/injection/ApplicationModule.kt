package com.dsokolov.baseapplication.yourapp.injection

import android.content.Context.MODE_PRIVATE
import androidx.room.Room
import com.dsokolov.baseapplication.yourapp.TestApplication
import com.dsokolov.baseapplication.yourapp.common.FlowProvider
import com.dsokolov.baseapplication.yourapp.common.Preferences
import com.dsokolov.baseapplication.yourapp.ApplicationHolder
import com.dsokolov.baseapplication.yourapp.room.Database
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule constructor(private val testApplication: TestApplication) {
    @Provides
    @Singleton
    fun provideFlowProvider(): FlowProvider {
        return FlowProvider()
    }

    @Provides
    @Singleton
    fun providePreferences(): Preferences {
        return Preferences(testApplication.getSharedPreferences(TestApplication.TAG, MODE_PRIVATE))
    }

    @Provides
    @Singleton
    fun provideApplicationHolder(): ApplicationHolder = ApplicationHolder(testApplication)

    @Provides
    @Singleton
    fun provideDatabase() = Room.databaseBuilder(testApplication, Database::class.java, "Database").build()
}