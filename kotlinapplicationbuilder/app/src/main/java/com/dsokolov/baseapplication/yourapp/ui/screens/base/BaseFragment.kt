package com.dsokolov.baseapplication.yourapp.ui.screens.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dsokolov.baseapplication.yourapp.logger.AppLogger

abstract class BaseFragment: Fragment() {
    protected val TAG = this.javaClass.name

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AppLogger.d(TAG, "onAttach")
    }

    override fun onDetach() {
        AppLogger.d(TAG, "onDetach")
        super.onDetach()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppLogger.d(TAG, "onCreate")
    }

    override fun onDestroy() {
        AppLogger.d(TAG, "onDestroy")
        super.onDestroy()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AppLogger.d(TAG, "onCreateView")
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        AppLogger.d(TAG, "onViewCreated")
    }

    override fun onDestroyView() {
        AppLogger.d(TAG, "onDestroyView")
        super.onDestroyView()
    }

    override fun onStart() {
        super.onStart()
        AppLogger.d(TAG, "onStart")
    }

    override fun onStop() {
        AppLogger.d(TAG, "onStop")
        super.onStop()
    }

    override fun onPause() {
        AppLogger.d(TAG, "onPause")
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        AppLogger.d(TAG, "onResume")
    }
}