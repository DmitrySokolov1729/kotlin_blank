package com.dsokolov.baseapplication.yourapp.models

import android.os.Parcelable

abstract class ParcelableBaseUserModel : BaseUserModel(), Parcelable