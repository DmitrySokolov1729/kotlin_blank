package com.dsokolov.baseapplication.yourapp.network.gson

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Friends {
    @SerializedName("response")
    @Expose
    var response: Response? = null
}

class Response {
    @SerializedName("count")
    @Expose
    var count: Int? = null

    @SerializedName("items")
    @Expose
    var contacts: List<Contact>? = null
}

@Entity
class Contact {
    @SerializedName("first_name")
    @Expose
    var firstName: String? = null

    @PrimaryKey
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("last_name")
    @Expose
    var lastName: String? = null

    @SerializedName("nickname")
    @Expose
    var nickname: String? = null

    @SerializedName("photo_200_orig")
    @Expose
    var photo200Orig: String? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Contact) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id ?: 0
    }
}