package com.dsokolov.baseapplication.yourapp.injection

import com.dsokolov.baseapplication.yourapp.ApplicationHolder
import com.dsokolov.baseapplication.yourapp.TestApplication
import com.dsokolov.baseapplication.yourapp.ui.screens.ContactsViewModel
import dagger.Component
import javax.inject.Singleton

@Component(modules = [ApplicationModule::class, NetworkModule::class])
@Singleton
interface TestApplicationComponent {
    fun inject (testApplication: TestApplication)
    fun inject (applicationHolder: ApplicationHolder)
    fun inject (contactsViewModel: ContactsViewModel)
}