package com.dsokolov.baseapplication.yourapp.ui.screens

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.dsokolov.baseapplication.R
import com.dsokolov.baseapplication.yourapp.ui.screens.base.BaseActivity
import com.dsokolov.baseapplication.databinding.ActivityMainBinding
import com.dsokolov.baseapplication.yourapp.logger.AppLogger
import com.dsokolov.baseapplication.yourapp.ApplicationHolder
import com.vk.api.sdk.VK
import com.vk.api.sdk.auth.VKAccessToken
import com.vk.api.sdk.auth.VKAuthCallback
import com.vk.api.sdk.auth.VKScope


class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private val contactsViewModel by viewModels<ContactsViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.navigation) as NavHostFragment
        navController = navHostFragment.navController

        if (!VK.isLoggedIn()) {
            login()
        } else {
            setAuthorization(true)
        }

        val logoutObserver = Observer<Boolean> {
            AppLogger.d(TAG, "Incoming logout event")
            login()
        }

        contactsViewModel.login.observe(this, logoutObserver)
    }

    private fun login() {
        setEmpty()
        VK.login(this, arrayListOf(VKScope.FRIENDS, VKScope.PHOTOS, VKScope.PHONE, VKScope.OFFLINE))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val callback = object: VKAuthCallback {
            override fun onLogin(token: VKAccessToken) {
                ApplicationHolder.instance.saveToken(token.accessToken)
                setAuthorization(true)
            }

            override fun onLoginFailed(errorCode: Int) {
                setAuthorization(false)
            }
        }

        if (data == null || !VK.onActivityResult(requestCode, resultCode, data, callback)) {
            setAuthorization(VK.isLoggedIn())
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun setAuthorization(isAuthorization: Boolean) {
        navController.currentDestination?.let {
            if (it.id == R.id.loadingFragment) {
                val action = if (isAuthorization) {
                    R.id.action_emptyFragment_to_contactsFragment
                } else {
                    R.id.action_emptyFragment_to_noAuthorizationFragment
                }

                navController.navigate(action)
            } else {
                AppLogger.d(TAG, "attempt to navigation when is not loading state")
            }
        }
    }

    private fun setEmpty() {
        navController.popBackStack()
        navController.navigate(R.id.loadingFragment)
    }
}