package com.dsokolov.baseapplication.yourapp.injection

import com.dsokolov.baseapplication.yourapp.network.VkService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {
    @Provides
    @Singleton
    fun provideVkService() : VkService {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client = OkHttpClient.Builder().addInterceptor(interceptor)

        return Retrofit.Builder()
            .baseUrl(VK_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client.build())
            .build()
            .create(VkService::class.java)
    }

    companion object {
        const val VK_URL = "https://api.vk.com/method/"
    }
}