package com.dsokolov.baseapplication.yourapp.utils

import android.os.SystemClock
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    private val dateFormatter = SimpleDateFormat("yyyy-MM-dd_HH:mm:ss.SSS", Locale.ENGLISH)

    fun convertTimeToString(date: Long) = dateFormatter.format(date)
    fun getSystemElapsedRealTime() = SystemClock.elapsedRealtime()
    fun getCurrentTime() = System.currentTimeMillis()
}