package com.dsokolov.baseapplication.yourapp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.dsokolov.baseapplication.databinding.ContactItemBinding
import com.dsokolov.baseapplication.yourapp.network.gson.Contact

class ContactsAdapter : ListAdapter<Contact, ContactsAdapter.ViewHolder>(diffCallback) {
    companion object {
        val diffCallback = object : DiffUtil.ItemCallback<Contact>() {
            override fun areItemsTheSame(oldContact: Contact, newContact: Contact): Boolean {
                return oldContact === newContact
            }

            override fun areContentsTheSame(oldContact: Contact, newContact: Contact): Boolean {
                return oldContact.id == newContact.id
            }
        }
    }
    inner class ViewHolder(val binding: ContactItemBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ContactItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = getItem(position)

            item.firstName?.let { holder.binding.firstName.setText(it) }
            item.lastName?.let { holder.binding.lastName.setText(it) }
            item.photo200Orig?.let { Glide.with(holder.binding.root.context)
                .load(it).apply(RequestOptions().circleCrop()).into(holder.binding.imageContact) }

    }
}

