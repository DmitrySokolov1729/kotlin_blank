package com.dsokolov.baseapplication.yourapp.ui.screens.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dsokolov.baseapplication.yourapp.logger.AppLogger

abstract class BaseActivity : AppCompatActivity() {
    protected val TAG = this.javaClass.name

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppLogger.d(TAG, "onCreate")
    }

    override fun onDestroy() {
        AppLogger.d(TAG, "onDestroy")
        super.onDestroy()
    }

    override fun onStart() {
        super.onStart()
        AppLogger.d(TAG, "onStart")
    }

    override fun onStop() {
        AppLogger.d(TAG, "onStop")
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        AppLogger.d(TAG, "onResume")
    }

    override fun onPause() {
        AppLogger.d(TAG, "onPause")
        super.onPause()
    }
}