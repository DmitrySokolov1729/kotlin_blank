package com.dsokolov.baseapplication.yourapp

import android.content.BroadcastReceiver
import android.content.IntentFilter
import com.dsokolov.baseapplication.yourapp.common.FlowProvider
import com.dsokolov.baseapplication.yourapp.common.Preferences
import com.dsokolov.baseapplication.yourapp.logger.AppLogger
import com.vk.api.sdk.VK
import com.vk.api.sdk.VKTokenExpiredHandler
import javax.inject.Inject
import javax.inject.Singleton

@Singleton class ApplicationHolder (private val application: TestApplication) {

    @Inject
    lateinit var preferences: Preferences

    @Inject
    lateinit var flowProvider: FlowProvider

    companion object {
        lateinit var instance: ApplicationHolder private set
    }

    init {
        instance = this
        getComponent().inject(this)
        AppLogger.d(TestApplication.TAG, "ApplicationHolder was created hashCode = " + hashCode()
                + ", preferences hashCode = " + preferences.hashCode()
                + ", flowProvider.hashCode() = " + flowProvider.hashCode())
        VK.addTokenExpiredHandler(handler = object : VKTokenExpiredHandler {
            override fun onTokenExpired() {
                AppLogger.d(TestApplication.TAG, "onTokenExpired")
                preferences.saveToken(null)
                flowProvider.loginFlow.tryEmit(false)
            }
        })
    }

    private fun getContext() = application.applicationContext
    fun getPackageManager() = getContext().packageManager
    fun getResources() = getContext().resources
    fun getSelfPermissionStatus(name: String) = getContext().checkSelfPermission(name)
    fun getSystemService(serviceName: String): Any? = getContext().getSystemService(serviceName)
    fun registerReceiver(broadcastReceiver: BroadcastReceiver?, intentFilter: IntentFilter) = getContext()
            .registerReceiver(broadcastReceiver, intentFilter)
    fun unregisterReceiver(broadcastReceiver: BroadcastReceiver) = getContext()
            .unregisterReceiver(broadcastReceiver)
    fun getContentResolver() = getContext().contentResolver
    fun getPackageName() = getContext().packageName
    fun saveToken(token: String) {
        preferences.saveToken(token)
    }
    fun getComponent() = application.getComponent()
}