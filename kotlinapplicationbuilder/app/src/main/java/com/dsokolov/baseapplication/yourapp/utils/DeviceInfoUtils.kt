package com.dsokolov.baseapplication.yourapp.utils

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.content.pm.PermissionGroupInfo
import android.content.pm.PermissionInfo
import android.os.BatteryManager
import android.os.Build
import android.os.Environment
import android.os.StatFs
import android.provider.Settings
import android.provider.Settings.SettingNotFoundException
import android.telephony.TelephonyManager
import com.dsokolov.baseapplication.yourapp.ApplicationHolder
import com.dsokolov.baseapplication.yourapp.logger.AppLogger
import java.lang.IllegalArgumentException
import java.net.InetAddress
import java.net.NetworkInterface
import java.net.SocketException
import java.util.*


object DeviceInfoUtils {
    const val UNKNOWN = "UNKNOWN"
    private val TAG = DeviceInfoUtils.javaClass.name
    private const val ERR_CODE = -1
    private const val OS_VERSION = "os.version"
    private const val DOT = "."
    private const val INVALID = -1

    fun getOSVersionName() = Build.VERSION.RELEASE
    fun getOSBuildNumber() = Build.ID + DOT + Build.VERSION.INCREMENTAL
    fun getKernelVersion() = System.getProperty(OS_VERSION)

    fun getRAMSize(): Long {
        val activityManager: Any? = ApplicationHolder.instance.getSystemService(Context.ACTIVITY_SERVICE)
        val memoryInfo: ActivityManager.MemoryInfo = ActivityManager.MemoryInfo()

        if (activityManager != null && activityManager is ActivityManager) {
            activityManager.getMemoryInfo(memoryInfo)
        }

        return memoryInfo.availMem
    }
    fun getExternalDiscSize(): String {
        val path = Environment.getExternalStorageState()
        return if (path == null) {
            UNKNOWN
        } else {
            try {
                val statFs = StatFs(path)
                val bytesAvailable = statFs.blockSizeLong * statFs.availableBlocksLong
                bytesAvailable.toString()
            } catch (e: IllegalArgumentException) {
                AppLogger.e(TAG, "getExternalDiscSize ", e)
                UNKNOWN
            }
        }
    }
    fun getInternalDiscSize(): String {
        val file = Environment.getDataDirectory()
        return if (file == null) {
            UNKNOWN
        } else {
            try {
                val statFs = StatFs(file.path)
                val bytesAvailable = statFs.blockSizeLong * statFs.availableBlocksLong
                bytesAvailable.toString()
            } catch (e: IllegalArgumentException) {
                AppLogger.e(TAG, "getExternalDiscSize ", e)
                UNKNOWN
            }
        }
    }
    fun getDeviceModel() = Build.MODEL
    fun getBatteryLevel(): Int {
        val batteryIntent = ApplicationHolder.instance
                .registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
        if (batteryIntent != null) {
            val level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
            val scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
            return level / scale * 100
        } else {
            return INVALID
        }
    }
    fun getPoweredOnDeviceTime() = DateUtils.convertTimeToString(DateUtils.getCurrentTime()
            - DateUtils.getSystemElapsedRealTime())
    fun getMACAddress(): String {
        try {
            val interfaces: List<NetworkInterface?> = Collections.list(NetworkInterface.getNetworkInterfaces())
            for (intr in interfaces) {
                if (intr == null) continue
                var mac: ByteArray?
                try {
                    mac = intr.hardwareAddress
                } catch (e: SocketException) {
                    AppLogger.e(TAG, "getMACAddress error on trying intr.hardwareAddress", e)
                    continue
                }

                if (mac == null) continue

                val buf = StringBuilder()
                for (b in mac) buf.append(String.format("%02X:", b))
                if (buf.isNotEmpty()) buf.deleteCharAt(buf.length - 1)
                return buf.toString()
            }
        } catch (e: SocketException) {
            AppLogger.e(TAG, "getMACAddress error on trying NetworkInterface.getNetworkInterfaces()", e)
            return UNKNOWN
        } catch (e: NullPointerException) {
            AppLogger.e(TAG, "We've got NullPointerException when trying obtain getNetworkInterfaces", e)
        }

        return UNKNOWN
    }
    fun getIpAddress(): String {
        try {
            val interfaces: List<NetworkInterface?> = Collections.list(NetworkInterface.getNetworkInterfaces())
            for (intr in interfaces) {
                if (intr == null) continue
                val enumIpAddr: Enumeration<InetAddress?> = intr.inetAddresses
                while (enumIpAddr.hasMoreElements()) {
                    val inetAddress = enumIpAddr.nextElement()
                    if (inetAddress != null && !inetAddress.isLoopbackAddress) {
                        return inetAddress.hostAddress
                    }
                }
            }
        } catch (e: SocketException) {
            AppLogger.e(TAG, "getIpAddress error on trying NetworkInterface.getNetworkInterfaces()", e)
            return UNKNOWN
        }
        catch (e: NullPointerException) {
            AppLogger.e(TAG, "We've got NullPointerException when trying obtain getNetworkInterfaces", e)
        }

        return UNKNOWN
    }
    fun getOperatorName(): String {
        val telephonyManager: Any? = ApplicationHolder.instance.getSystemService(Context.TELEPHONY_SERVICE)
        if (telephonyManager != null && telephonyManager is TelephonyManager) {
            return telephonyManager.networkOperatorName
        }

        return UNKNOWN
    }
    fun isDataRoamingEnabled(): Boolean {
        return try {
            Settings.Secure.getInt(ApplicationHolder.instance.getContentResolver(), Settings.Global.DATA_ROAMING) == 1
        } catch (e: SettingNotFoundException) {
            AppLogger.e(TAG, "isDataRoamingEnabled", e)
            false
        }
    }
    fun isMobileDataEnabled(): Boolean {
        return try {
            Settings.Secure.getInt(ApplicationHolder.instance.getContentResolver(), "mobile_data", 1) == 1
        } catch (e: NullPointerException) {
            AppLogger.e(TAG, "isMobileDataEnabled() NullPointerException", e)
            false
        }
    }
    fun isMicrophoneAvailable(): Boolean = ApplicationHolder.instance.getPackageManager()
            .hasSystemFeature(PackageManager.FEATURE_MICROPHONE)
    fun getAppVersionName(): String {
        return try {
            ApplicationHolder.instance.getPackageManager()
                .getPackageInfo(ApplicationHolder.instance.getPackageName(), 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            UNKNOWN
        }
    }
    @Suppress("DEPRECATION")
    fun getVersionCode(): Int {
        return try {
            ApplicationHolder.instance.getPackageManager()
                    .getPackageInfo(ApplicationHolder.instance.getPackageName(), 0).versionCode
        } catch (e: PackageManager.NameNotFoundException) {
            ERR_CODE
        }
    }
    fun getDevice() = Build.DEVICE
    fun getBrand() = Build.BRAND
    fun getProduct() = Build.PRODUCT

    fun checkAllPermissions(): String {
        val permissions = getAllPermissions()
        val sb = StringBuilder()
        for (permission in permissions) {
            if (permission != null) {
                val name = permission.name
                val status = ApplicationHolder.instance.getSelfPermissionStatus(name)
                sb.append(name).append(" : ")
                        .append(if (status == PackageManager.PERMISSION_GRANTED) "Enabled" else "Disabled")
                        .append("\n")
            }
        }
        return sb.toString()
    }
    fun getAllPermissions(): List<PermissionInfo?> {
        val pm: PackageManager = ApplicationHolder.instance.getPackageManager()
        val permissionsGroup: MutableList<PermissionGroupInfo?> = pm.getAllPermissionGroups(0)
        val permissions: MutableList<PermissionInfo?> = LinkedList()
        for (permissionGroup in permissionsGroup) {
            if (permissionGroup != null) {
                try {
                    permissions.addAll(pm.queryPermissionsByGroup(permissionGroup.name, 0))
                } catch (ignored: PackageManager.NameNotFoundException) {
                }
            }
        }
        return permissions
    }
}