package com.dsokolov.baseapplication.yourapp.common

import kotlinx.coroutines.flow.MutableSharedFlow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton class FlowProvider @Inject constructor() {
    val loginFlow = MutableSharedFlow<Boolean>(1)
}