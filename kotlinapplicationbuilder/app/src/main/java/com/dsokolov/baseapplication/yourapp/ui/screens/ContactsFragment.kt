package com.dsokolov.baseapplication.yourapp.ui.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import com.dsokolov.baseapplication.R
import com.dsokolov.baseapplication.databinding.ContactsBinding
import com.dsokolov.baseapplication.yourapp.network.gson.Contact
import com.dsokolov.baseapplication.yourapp.ui.adapter.ContactsAdapter
import com.dsokolov.baseapplication.yourapp.ui.screens.base.BaseFragment
import com.vk.api.sdk.VK

class ContactsFragment : BaseFragment() {
    private var binding: ContactsBinding? = null
    private val contactsViewModel: ContactsViewModel by activityViewModels()

    private val contactsAdapter = ContactsAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ContactsBinding.inflate(inflater, container, false)
        binding!!.rvContacts.adapter = contactsAdapter

        val itemDecorator = DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
        itemDecorator.setDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.line_devider)!!)
        binding!!.rvContacts.addItemDecoration(itemDecorator)

        val friendsObserver = Observer<List<Contact>> {
            contactsAdapter.submitList(it)
        }
        contactsViewModel.friends.observe(viewLifecycleOwner, friendsObserver)

        if (VK.isLoggedIn()) {
            contactsViewModel.loadFriends()
        }

        return binding!!.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}