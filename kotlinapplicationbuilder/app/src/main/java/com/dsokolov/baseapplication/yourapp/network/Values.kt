package com.dsokolov.baseapplication.yourapp.network

import com.vk.api.sdk.VK

val FRIENDS_FIELD = "photo_200_orig,nickname"
val VK_VERSION = VK.getApiVersion()