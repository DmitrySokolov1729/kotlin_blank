package com.dsokolov.baseapplication.yourapp.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dsokolov.baseapplication.yourapp.network.gson.Contact

@Database(entities = [Contact::class], version = 1)
abstract class Database : RoomDatabase() {
    abstract fun contactDao() : ContactDao
}