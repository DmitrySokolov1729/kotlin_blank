package com.dsokolov.baseapplication.yourapp

import android.app.Application
import com.dsokolov.baseapplication.yourapp.logger.AppLogger
import com.dsokolov.baseapplication.yourapp.logger.LogConfig
import com.dsokolov.baseapplication.yourapp.utils.DeviceInfoUtils
import com.dsokolov.baseapplication.yourapp.injection.ApplicationModule
import com.dsokolov.baseapplication.yourapp.injection.DaggerTestApplicationComponent
import com.dsokolov.baseapplication.yourapp.injection.NetworkModule
import com.dsokolov.baseapplication.yourapp.injection.TestApplicationComponent
import javax.inject.Inject

class TestApplication: Application() {
    private val testApplicationComponent: TestApplicationComponent by lazy {
        DaggerTestApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .networkModule(NetworkModule())
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        AppLogger.startLogger(logConfig(), TAG)
        testApplicationComponent.inject(this)
        logDeviceInfo()
    }

    @Inject fun onInject(applicationHolder: ApplicationHolder) {
        AppLogger.d(TAG, "ApplicationHolder was injected " + applicationHolder.hashCode())
    }

    fun getComponent(): TestApplicationComponent {
        return testApplicationComponent
    }

    fun logConfig(): LogConfig {
        return LogConfig("com.dsokolov.baseapplication",
            filesDir.absolutePath,
            TAG + "_%g.txt",
            64,
            250000)
    }

    fun logDeviceInfo() {
        AppLogger.i(TAG, "OSVersionName " + DeviceInfoUtils.getOSVersionName())
        AppLogger.i(TAG, "OSBuildNumber " + DeviceInfoUtils.getOSBuildNumber())
        AppLogger.i(TAG, "KernelVersion " + DeviceInfoUtils.getKernelVersion())
        AppLogger.i(TAG, "RAMSize " + DeviceInfoUtils.getRAMSize())
        AppLogger.i(TAG, "ExternalDiscSize " + DeviceInfoUtils.getExternalDiscSize())
        AppLogger.i(TAG, "InternalDiscSize " + DeviceInfoUtils.getInternalDiscSize())
        AppLogger.i(TAG, "DeviceModel " + DeviceInfoUtils.getDeviceModel())
        AppLogger.i(TAG, "BatteryLevel " + DeviceInfoUtils.getBatteryLevel())
        AppLogger.i(TAG, "PoweredOnDeviceTime " + DeviceInfoUtils.getPoweredOnDeviceTime())
        AppLogger.i(TAG, "MACAddress " + DeviceInfoUtils.getMACAddress())
        AppLogger.i(TAG, "IpAddress " + DeviceInfoUtils.getIpAddress())
        AppLogger.i(TAG, "OperatorName " + DeviceInfoUtils.getOperatorName())
        AppLogger.i(TAG, "DataRoamingEnabled " + DeviceInfoUtils.isDataRoamingEnabled())
        AppLogger.i(TAG, "MobileDataEnabled " + DeviceInfoUtils.isMobileDataEnabled())
        AppLogger.i(TAG, "MicrophoneAvailable " + DeviceInfoUtils.isMicrophoneAvailable())
        AppLogger.i(TAG, "AppVersionName " + DeviceInfoUtils.getAppVersionName())
        AppLogger.i(TAG, "VersionCode " + DeviceInfoUtils.getVersionCode())
        AppLogger.i(TAG, "Device " + DeviceInfoUtils.getDevice())
        AppLogger.i(TAG, "Brand " + DeviceInfoUtils.getBrand())
        AppLogger.i(TAG, "Product " + DeviceInfoUtils.getProduct())
        AppLogger.i(TAG, "AllPermissions " + DeviceInfoUtils.checkAllPermissions())
    }

    companion object {
        const val TAG = "com.dsokolov.baseapplication.yourapp.TestApplication"
    }
}