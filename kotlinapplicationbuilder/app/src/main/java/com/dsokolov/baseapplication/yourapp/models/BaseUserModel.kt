package com.dsokolov.baseapplication.yourapp.models

abstract class BaseUserModel {
    abstract fun getType(): String
}