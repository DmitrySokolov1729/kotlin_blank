package com.dsokolov.baseapplication.yourapp.network

import com.dsokolov.baseapplication.yourapp.network.gson.Friends
import retrofit2.http.GET
import retrofit2.http.Query

interface VkService {
    @GET("friends.get")
    suspend fun getFriends(@Query("user_id") userId: Int,
                           @Query("access_token") accessToken: String,
                           @Query("fields") fields: String = FRIENDS_FIELD,
                           @Query("v") version: String = VK_VERSION): Friends
}