package com.dsokolov.baseapplication.yourapp.ui.screens

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dsokolov.baseapplication.yourapp.common.FlowProvider
import com.dsokolov.baseapplication.yourapp.common.Preferences
import com.dsokolov.baseapplication.yourapp.logger.AppLogger
import com.dsokolov.baseapplication.yourapp.network.VkService
import com.dsokolov.baseapplication.yourapp.ApplicationHolder
import com.dsokolov.baseapplication.yourapp.network.gson.Contact
import com.dsokolov.baseapplication.yourapp.room.Database
import com.vk.api.sdk.VK
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

class ContactsViewModel : ViewModel() {
    val TAG = "com.dsokolov.baseapplication.yourapp.ui.screens.ContactsViewModel"
    @Inject
    lateinit var vkService: VkService
    @Inject
    lateinit var flowProvider: FlowProvider
    @Inject
    lateinit var preferences: Preferences
    @Inject
    lateinit var database: Database

    private val _friends: MutableLiveData<List<Contact>> by lazy {
        MutableLiveData<List<Contact>>()
    }
    private val _login: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }
    var friends: LiveData<List<Contact>> = _friends
    var login: LiveData<Boolean> = _login

    init {
        ApplicationHolder.instance.getComponent().inject(this)
        viewModelScope.launch(Dispatchers.Main) {
            flowProvider.loginFlow.collect {
                _login.value = it
            }
        }
    }

    fun loadFriends() {
        viewModelScope.launch(Dispatchers.IO) {

            val jobLoadFromInternet = async {
                preferences.getToken()?.let {
                    vkService.getFriends(
                        VK.getUserId(), it
                    )
                }
            }

            val jobLoadFromDb = async {
                database.contactDao().getAll().apply {
                    AppLogger.d(TAG, "contacts from DB will be submitted")
                    submitContacts(this)
                }
            }

            val friendsFromInternet = jobLoadFromInternet.await()
            if (friendsFromInternet != null) {
                val contactsFromInternet: List<Contact>? = friendsFromInternet.response?.contacts
                val contactsFromDb = jobLoadFromDb.await()

                AppLogger.d(
                    TAG,
                    "contactsFromInternet.size ${contactsFromInternet?.size}, contactsFromDb.size ${contactsFromDb.size}"
                )

                val wasUpdated = contactsFromDb != contactsFromInternet
                if (wasUpdated) {
                    AppLogger.d(TAG, "contacts from DB and from internet are different")
                    contactsFromInternet?.let {
                        AppLogger.d(TAG, "contacts from internet will be submitted")
                        submitContacts(it)
                        database.runInTransaction {
                            database.contactDao().deleteAll(contactsFromDb)
                            database.contactDao().insertAll(it)
                        }
                    }
                } else {
                    AppLogger.d(TAG, "contacts from DB and from internet are the same")
                }
            }
        }
    }

    fun login() {
        _login.value = true
    }

    private suspend fun submitContacts(contacts: List<Contact>) {
        withContext(Dispatchers.Main) {
            _friends.value = contacts
        }
    }
}