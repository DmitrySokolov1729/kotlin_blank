package com.dsokolov.baseapplication.yourapp.room

import androidx.room.*
import com.dsokolov.baseapplication.yourapp.network.gson.Contact

@Dao
interface ContactDao {
    @Query("SELECT * FROM contact")
    fun getAll(): List<Contact>

    @Query("SELECT * FROM contact WHERE id = :id")
    fun getById(id: Int): Contact

    @Insert
    fun insert(contact: Contact)

    @Insert
    fun insertAll(contacts: List<Contact>)

    @Update
    fun update(contact: Contact)

    @Delete
    fun delete(contact: Contact)

    @Delete
    fun deleteAll(contacts: List<Contact>)
}