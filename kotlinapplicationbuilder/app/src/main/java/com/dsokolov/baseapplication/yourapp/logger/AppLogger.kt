package com.dsokolov.baseapplication.yourapp.logger

import android.util.Log
import com.dsokolov.baseapplication.BuildConfig
import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.io.InputStreamReader
import java.util.concurrent.atomic.AtomicBoolean
import java.util.logging.*
import java.util.logging.Logger

object AppLogger {
    private lateinit var mLogDir: File

    private const val LOG_DIR_NAME = "logs"
    private const val NEW_LINE = "\r\n"
    private val BUILD_HASH = if (BuildConfig.GIT_SHA.length >= 7) BuildConfig.GIT_SHA.substring(
        0,
        7
    ) else BuildConfig.GIT_SHA
    private const val EXCEPTION_TRACES = "::EXCEPTION_TRACES"
    private const val COLON = ":"
    private const val OPEN_BRACKET = " ["
    private const val CLOSE_BRACKET = "] "

    private lateinit var applicationTag: String
    private val isStarted = AtomicBoolean(false)

    fun startLogger(logConfig: LogConfig, applicationTag: String) {
        Log.d(applicationTag, "startLogger")

        if (!isStarted.getAndSet(true)) {
            this.applicationTag = applicationTag

            mLogDir = File(logConfig.rootPath, LOG_DIR_NAME)
            val isLogsDirectoryExists: Boolean = if (!mLogDir.exists()) {
                mLogDir.mkdirs()
            } else {
                true
            }

            if (isLogsDirectoryExists) {
                val logger = Logger.getLogger(logConfig.applicationPackageName)
                Thread {
                    try {
                        val pattern = mLogDir.absolutePath + File.separator + logConfig.pattern
                        val fileHandler = FileHandler(
                                pattern, logConfig.fileSize,
                                logConfig.fileCount, true
                        )
                        fileHandler.formatter = LineFormatter()
                        logger.level = Level.FINER
                        logger.addHandler(fileHandler)
                        val process = Runtime.getRuntime().exec("logcat")
                        val bufferedReader = BufferedReader(InputStreamReader(process.inputStream))
                        var line: String?
                        while (bufferedReader.readLine().also { line = it } != null) {
                            logger.finer(line)
                        }
                    } catch (e: IOException) {
                        isStarted.set(false)
                        Log.e(applicationTag, "Logger was stopped with error", e)
                    }
                }.start()
            } else {
                Log.d(
                        applicationTag,
                        "initLogger::logs folder can't be created " + mLogDir.absolutePath
                )
            }
        }
    }

    fun v(messageTag: String, message: String) {
        Log.v(applicationTag, formatMessage(messageTag, message, null))
    }

    fun d(messageTag: String, message: String) {
        Log.d(applicationTag, formatMessage(messageTag, message, null))
    }

    fun i(messageTag: String, message: String) {
        Log.i(applicationTag, formatMessage(messageTag, message, null))
    }

    fun w(messageTag: String, message: String) {
        Log.w(applicationTag, formatMessage(messageTag, message, null))
    }

    fun e(messageTag: String, message: String, e: Throwable?) {
        Log.e(applicationTag, formatMessage(messageTag, message, e), e)
    }

    fun provideLogPath(): String {
        return mLogDir.absolutePath
    }

    private fun formatMessage(messageTag: String, message: String, e: Throwable?): String {
        val sb = StringBuilder(100)
        sb.append(BUILD_HASH).append(COLON)
            .append(OPEN_BRACKET).append(messageTag).append(CLOSE_BRACKET)
            .append(message)
        if (e != null) {
            sb.append(NEW_LINE).append(EXCEPTION_TRACES)
            sb.append(e.message)
            for (ste in e.stackTrace) {
                sb.append(NEW_LINE).append(ste.toString())
            }
        }
        return sb.toString()
    }

    class LineFormatter : Formatter() {
        override fun format(rec: LogRecord): String {
            return rec.message + NEW_LINE
        }
    }
}